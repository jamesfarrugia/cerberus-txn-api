# RESTful API for money transfers

## Basic overview
The API is a simple Spring Boot app that reads config from the properties file 
for the active profile.  The API exposes simple endpoints primarily for transfers
but also for checking the balances.  This is in line with the assumption that 
the call originates from a trusted server, so we may assume that a balance check
may be necessary, and should be available to this trusted server.

Due to the assumed legal auditability as well as technical scalability 
requirements, the system follows a CQRS pattern, so each event is stored and
a view of the current account state may be used for querying.

## API Spec

### Data models
The models for this API are fairly simple, comprising of an account, a transfer,
command for transfer, and queries for accounts and transfers.

* The account balance is based on the base currency of the API operator, assumed
to be EUR.  The balance is represented in the lowest currency denominations, i.e.
cents (assuming that transfers are not related to shares/forex - that would 
require millis).  This allows for precise transactions and avoids any floating
point arithmetic limitations.
* The transfer contains the source and destination account numbers, internal IDs
a fee, amount in the currency of the transfer (same as source or destination)
the amount of the transfer in the currency of the transfer, and a status code.
* A 'create transfer' model defines the basic fields in a transfer and initiates
the record creation.


#### Assumptions
* The system assumes that an account can only be held by one account holder.  In
case of joint accounts, we would need a better relationship model that represents
the ownership, however in this API the account is held directly by one user,
although the user may hold multiple accounts.
* Transfer clearing, authorisations, etc are not considered, so the accounts
have a single, current balance and no book balance.
* A transfer fee is added for completeness, and this is assumed to be 
payer-deductible;
* The currency rates are not real, and as such the currency service always
returns a rate of 1.  This can easily be implemented, however is not within the
scope.

#### Other points
* The system may be extended to limit or impose fees if the transfer is made
between accounts of different types.  The types are defined in the DB however 
this is not implemented as a feature;
* The account creation and closure are not considered for simplicity, however both
are just commands that will eventually create or remove the account.  The rest
of the lifecycle of an account is inward and outward transfers.

## Implementation

### Transfer flow
The system can be extended by implementing and deploying transfer stop-points,
where the system may receive a new transfer and check any of its values so
that the transfer may be set as pending rather than fully autorising it.


## Building
The app can be built in a standard maven `mvn package` command, which will
produce the spring fat-jar in the target directory.

## Running
Calling `java -jar cerberus-txn-api-0.0.1.jar` fires up the application and
listens on port 8080.  By default the app is looking for the MySQL database
at 10.0.0.213:3306.  Before running the test, all that is needed is a simple 
MySQL server with a database instance named `cerberus` with a user capable of 
creating tables in the database.  There is no need for tables to be created, 
as the application handles them by itself.

A properties file named `application-<PROFILE>.properties` should be created in 
the same location as the jar and should contain:
* spring.datasource.url=jdbc:mysql://<IP>:<PORT>/<DB>
* spring.datasource.username=<USER>
* spring.datasource.password=<PASS>

Once the DB is running call 
`java -jar -Dspring.profiles.active=<PROFILE> cerberus-txn-api-0.0.1.jar`
to start the API.

## Running the test
The bash script `run-test.sh` will present details of two accounts, then it will
make a small transfer with no fee, and show the new state of the accounts,
then another small transfer but with a fee, and finally a large transfer that 
will fail and show the error.  Finally a list of transfer is shown for the first
account, two of which will be successful and an other that has failed (state = 1)

