package com.jf.crbs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main
 * @author james
 *
 */
@SpringBootApplication
public class FundTransfer
{
	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args)
	{
		SpringApplication.run(FundTransfer.class, args);
	}
}
