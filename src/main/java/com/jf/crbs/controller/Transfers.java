package com.jf.crbs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jf.crbs.exception.ApiException;
import com.jf.crbs.model.Transfer;
import com.jf.crbs.model.c.TransferCreation;
import com.jf.crbs.model.c.TransferUpdate;
import com.jf.crbs.model.q.AccountHistoryQuery;
import com.jf.crbs.model.q.TransferQuery;
import com.jf.crbs.service.TransfersService;

/**
 * The transfers controller handles the creation and querying of transfers
 * @author james
 *
 */
@RestController
@RequestMapping("transfers")
public class Transfers
{
	/** Autowired service for transfers */
	@Autowired private TransfersService transfers;
	
	/**
	 * Creates a new transfer by logging a transfer creation event
	 * 
	 * @param creation the creation command
	 * @return the created transfer
	 * @throws ApiException in case of a failed creation
	 */
	@RequestMapping(value = "transfer", method = RequestMethod.POST)
	public Transfer doCreateTransfer(
			@RequestBody TransferCreation creation)
	throws ApiException
	{
		creation.doValidate();
		return transfers.doCreateTransfer(creation);
	}
	
	/**
	 * Gets an individual transfer identified by the public transfer number
	 * 
	 * @param number the public transfer number
	 * @return the transfer if found
	 * @throws ApiException 
	 */
	@RequestMapping(value = "transfer/{number}", method = RequestMethod.GET)
	public Transfer getTransfer(
			@PathVariable("number") String number)
	throws ApiException
	{
		if (number == null || number.trim().isEmpty())
			throw new ApiException("Illegal parameter");
		
		TransferQuery query = new TransferQuery();
		query.setNumber(number);
		return transfers.getTransfer(query);
	}

	/**
	 * Updates an individual transfer identified by the public transfer number
	 * 
	 * @param number the public transfer number
	 * @param update the update to apply to the transfer
	 * @return the updated transfer if found and updated
	 * @throws ApiException in case the update fails
	 */
	@RequestMapping(value = "transfer/{number}", method = RequestMethod.POST)
	public Transfer doUpdateTransfer(
			@PathVariable("number") String number,
			@RequestBody TransferUpdate update)
	throws ApiException
	{
		update.doValidate();
		return transfers.doUpdateTransfer(number, update);
	}
	
	/**
	 * Gets an individual account identified by the public account number
	 * 
	 * @param number the public account number
	 * @return the account if found
	 * @throws ApiException in case the account is not found
	 */
	@RequestMapping(value = "account/{number}", method = RequestMethod.GET)
	public List<Transfer> getAccountTransfers(
			@PathVariable("number") String number)
	throws ApiException
	{
		if (number == null || number.trim().isEmpty())
			throw new ApiException("Illegal parameter");
		
		AccountHistoryQuery query = new AccountHistoryQuery();
		query.setNumber(number);
		return transfers.getAccountTransfers(query);
	}
}