package com.jf.crbs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jf.crbs.exception.ApiException;
import com.jf.crbs.model.Account;
import com.jf.crbs.model.q.AccountQuery;
import com.jf.crbs.service.AccountsService;

/**
 * The accounts controller handles the querying of accounts
 * @author james
 *
 */
@RestController
@RequestMapping("accounts")
public class Accounts
{
	/** Autowired service for accounts */
	@Autowired private AccountsService accounts;
	
	/**
	 * Gets an individual account identified by the public account number
	 * 
	 * @param number the public account number
	 * @return the account if found
	 * @throws ApiException in case the account is not found
	 */
	@RequestMapping(value = "account/{number}", method = RequestMethod.GET)
	public Account getAccount(
			@PathVariable("number") String number)
	throws ApiException
	{
		if (number == null || number.trim().isEmpty())
			throw new ApiException("Illegal parameter");
		
		AccountQuery query = new AccountQuery();
		query.setNumber(number);
		return accounts.getAccount(query);
	}
}