package com.jf.crbs.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.jf.crbs.exception.ApiException;
import com.jf.crbs.exception.BusinessException;
import com.jf.crbs.exception.ObjectNotFoundException;


/**
 * Servlet handler for managing bad request cases.  This avoids the default
 * Spring message and page.
 * 
 * @author james
 *
 */
@ControllerAdvice
class RequestExceptionHandler
{
	/** Class logger */
	private Logger log;
	
	/**
	 * Constructor.
	 * 
	 * Initialises the class logger
	 */
	public RequestExceptionHandler()
	{
		log = LoggerFactory.getLogger(getClass());
	}
	
	/**
	 * Internal exception handler to transform exception page into a usable 
	 * message.
	 * 
	 * @param exception the exception to handle
	 * @param title error title
	 * @param description error description
	 * @return a map for returning as an object
	 */
	private Map<String, Object> doHandle(Exception exception, String title, 
			String description)
	{
		log.trace("Exception encountered - Handling", exception);
		
		Map<String, Object> result = new HashMap<>();
		result.put("message", exception.getMessage());
		
		List<Map<String, String>> causeTree = new ArrayList<>();
		Throwable parent = exception;
		while (parent != null)
		{
			Map<String, String> cause = new HashMap<>();
			cause.put("message", parent.getMessage());
			cause.put("location", parent.getStackTrace()[0].toString());
			causeTree.add(cause);
			parent = parent.getCause();
		}
		
		result.put("cause-tree", causeTree);
		result.put("title", title);
		result.put("description", description);
		return result;
	}
	
	/**
	 * Handles the exception
	 * @param exception the exception to handle
	 * @return a string representing the response body
	 */
	@ExceptionHandler(ApiException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody Map<String, Object> handleServiceException(
			ApiException exception)
	{
		return doHandle(exception, "API Exception", 
				"The server could not process your request.");
	}
	
	/**
	 * Handles the exception
	 * @param exception the exception to handle
	 * @return a string representing the response body
	 */
	@ExceptionHandler(MissingServletRequestParameterException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody Map<String, Object> handleMissingParameter(
			MissingServletRequestParameterException exception)
	{
		return doHandle(exception, "Bad request", 
				"The server could not understand your request.");
	}
	
	/**
	 * Handles the exception
	 * @param exception the exception to handle
	 * @return a string representing the response body
	 */
	@ExceptionHandler(ObjectNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public @ResponseBody Map<String, ?> handleNotFoundError(
			ObjectNotFoundException exception)
	{
		return doHandle(exception, "Object Not Found",
				"The requested object could not be found.");
	}
	
	/**
	 * Handles the exception
	 * @param exception the exception to handle
	 * @return a string representing the response body
	 */
	@ExceptionHandler(BusinessException.class)
	@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
	public @ResponseBody Map<String, ?> handleBusinessError(
			BusinessException exception)
	{
		return doHandle(exception, "Business Error",
				"The requested operation could not be completed.");
	}
}