package com.jf.crbs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A transfer is a record of funds moving between two accounts on this same 
 * system.  The transfer needs a source and destination accounts, fee, status,
 * currency, and timestamp.
 * @author james
 *
 */
@Entity
@Table(name = "transfer")
public class Transfer
{
	/** Internal DB transfer ID */
	@JsonIgnore 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	/** Public transfer number */
	@NotBlank
	private String number;
	/** Public source account number */
	@NotBlank
	private String source;
	/** Public destination account number */
	@NotBlank
	private String destination;
	/** Transfer fee, deducted from the source account */
	private long fee;
	/** Amount of the transfer in the currency */
	private long amount;
	/** Conversion rate of the transfer */
	private float rate;
	/** Status of the transfer */
	private short status;
	/** Currency of payment, can either be as source or as destination.
	 * A single bit is enough to determine which one */
	private boolean ccyAsSource;
	/** Timestamp of transfer using UNIX TS in UTC*/
	private long timestamp;
	
	/**
	 * Default constructor for Transfer
	 */
	public Transfer()
	{
	}

	/**
	 * @return the id
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	/**
	 * @return the number
	 */
	public String getNumber()
	{
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number)
	{
		this.number = number;
	}

	/**
	 * @return the source
	 */
	public String getSource()
	{
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source)
	{
		this.source = source;
	}

	/**
	 * @return the destination
	 */
	public String getDestination()
	{
		return destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination)
	{
		this.destination = destination;
	}

	/**
	 * @return the fee
	 */
	public long getFee()
	{
		return fee;
	}

	/**
	 * @param fee the fee to set
	 */
	public void setFee(long fee)
	{
		this.fee = fee;
	}

	/**
	 * @return the amount
	 */
	public long getAmount()
	{
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the rate
	 */
	public float getRate()
	{
		return rate;
	}

	/**
	 * @param rate the rate to set
	 */
	public void setRate(float rate)
	{
		this.rate = rate;
	}

	/**
	 * @return the status
	 */
	public short getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(short status)
	{
		this.status = status;
	}

	/**
	 * @return the ccyAsSource
	 */
	public boolean isCcyAsSource()
	{
		return ccyAsSource;
	}

	/**
	 * @param ccyAsSource the ccyAsSource to set
	 */
	public void setCcyAsSource(boolean ccyAsSource)
	{
		this.ccyAsSource = ccyAsSource;
	}

	/**
	 * @return the timestamp
	 */
	public long getTimestamp()
	{
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(long timestamp)
	{
		this.timestamp = timestamp;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof Transfer))
			return false;
		
		return ((Transfer)obj).id == this.id;
	}
}
