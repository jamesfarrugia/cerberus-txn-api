package com.jf.crbs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * An Account is a representation of the record of a customer's account.
 * 
 * Details related to account such as creation date, currency, account type, etc
 * are stored here.
 * 
 * @author james
 *
 */
@Entity
@Table(name = "account")
public class Account
{
	/** Internal DB account ID */
	@JsonIgnore 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/** Internal DB ID of the account holder */
	@JsonIgnore
	private long accHolderId;
	/** Public account number */
	@NotBlank
	private String number;
	/** Currency code is stored in the DB as an ISO4217 code */
	@NotBlank
	private String ccy;
	/** Code of the account type */
	private short type;
	/** The current balance of the account in EUR cents.*/
	private long balance;

	/**
	 * Default constructor for Account
	 */
	public Account()
	{
	}

	/**
	 * @return the id
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	/**
	 * @return the accHolderId
	 */
	public long getAccHolderId()
	{
		return accHolderId;
	}

	/**
	 * @param accHolderId the accHolderId to set
	 */
	public void setAccHolderId(long accHolderId)
	{
		this.accHolderId = accHolderId;
	}

	/**
	 * @return the number
	 */
	public String getNumber()
	{
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number)
	{
		this.number = number;
	}

	/**
	 * @return the ccy
	 */
	public String getCcy()
	{
		return ccy;
	}

	/**
	 * @param ccy the ccy to set
	 */
	public void setCcy(String ccy)
	{
		this.ccy = ccy;
	}

	/**
	 * @return the type
	 */
	public short getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(short type)
	{
		this.type = type;
	}

	/**
	 * @return the balance
	 */
	public long getBalance()
	{
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(long balance)
	{
		this.balance = balance;
	}
}
