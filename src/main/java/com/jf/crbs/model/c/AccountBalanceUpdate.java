package com.jf.crbs.model.c;

/**
 * Command to alter the balance of an account
 * @author james
 *
 */
public class AccountBalanceUpdate
{
	/** Internal ID of the account to alter */
	private long id;
	/** Delta to apply to current balance */
	private long delta;
	
	/**
	 * Default constructor for AccountBalanceUpdate
	 */
	public AccountBalanceUpdate()
	{
	}

	/**
	 * @return the id
	 */
	public final long getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public final void setId(long id)
	{
		this.id = id;
	}

	/**
	 * @return the delta
	 */
	public final long getDelta()
	{
		return delta;
	}

	/**
	 * @param delta the delta to set
	 */
	public final void setDelta(long delta)
	{
		this.delta = delta;
	}
}
