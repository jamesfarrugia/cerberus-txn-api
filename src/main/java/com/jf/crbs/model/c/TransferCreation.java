package com.jf.crbs.model.c;

import com.jf.crbs.exception.BusinessException;

/**
 * The create transfer command logs a new transfer event and moves funds between
 * two accounts
 * @author james
 *
 */
public class TransferCreation
{
	/** Public source account number */
	private String source;
	/** Public destination account number */
	private String destination;
	/** Transfer fee, deducted from the source account */
	private long fee;
	/** Amount of the transfer in the currency */
	private long amount;
	/** Currency of payment, can either be as source or as destination.
	 * A single bit is enough to determine which one */
	private boolean ccyAsSource;
	
	/**
	 * Default constructor for CreateTransfer
	 */
	public TransferCreation()
	{
	}

	/**
	 * @return the source
	 */
	public final String getSource()
	{
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public final void setSource(String source)
	{
		this.source = source;
	}

	/**
	 * @return the destination
	 */
	public final String getDestination()
	{
		return destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public final void setDestination(String destination)
	{
		this.destination = destination;
	}

	/**
	 * @return the fee
	 */
	public final long getFee()
	{
		return fee;
	}

	/**
	 * @param fee the fee to set
	 */
	public final void setFee(long fee)
	{
		this.fee = fee;
	}

	/**
	 * @return the amount
	 */
	public final long getAmount()
	{
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public final void setAmount(long amount)
	{
		this.amount = amount;
	}

	/**
	 * @return the ccyAsSource
	 */
	public final boolean isCcyAsSource()
	{
		return ccyAsSource;
	}

	/**
	 * @param ccyAsSource the ccyAsSource to set
	 */
	public final void setCcyAsSource(boolean ccyAsSource)
	{
		this.ccyAsSource = ccyAsSource;
	}
	
	/**
	 * Validates this transfer creation command.  In case of a valid command,
	 * the method returns with out throwing exceptions
	 * @throws BusinessException in case of an error
	 */
	public void doValidate()
	throws BusinessException
	{
		if (this.source == null || this.source.trim().isEmpty())
			throw new BusinessException("Source account is required");
		if (this.destination == null || this.destination.trim().isEmpty())
			throw new BusinessException("Destination account is required");
		if (this.fee < 0)
			throw new BusinessException("Fee cannot be negative");
		if (this.amount < 0)
			throw new BusinessException("Amount cannot be negative");
	}
}
