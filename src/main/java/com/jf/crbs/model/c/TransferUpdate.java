package com.jf.crbs.model.c;

/**
 * The create transfer command logs a new transfer event and moves funds between
 * two accounts
 * @author james
 *
 */
public class TransferUpdate
{
	/** Transfer fee, deducted from the source account, which may be changed */
	private long fee;
	/** The new status of a transfer */
	private short status;
	
	/**
	 * Default constructor for CreateTransfer
	 */
	public TransferUpdate()
	{
	}

	/**
	 * @return the fee
	 */
	public final long getFee()
	{
		return fee;
	}

	/**
	 * @param fee the fee to set
	 */
	public final void setFee(long fee)
	{
		this.fee = fee;
	}

	/**
	 * @return the status
	 */
	public final short getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public final void setStatus(short status)
	{
		this.status = status;
	}
	
	/**
	 * Validates this transfer creation command.  In case of a valid command,
	 * the method returns with out throwing exceptions
	 */
	public void doValidate()
	{
		if (this.fee < 0)
			throw new IllegalArgumentException("Fee cannot be negative");
	}
}
