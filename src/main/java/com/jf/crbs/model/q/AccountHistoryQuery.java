package com.jf.crbs.model.q;

/**
 * Simple query that lists the transfers done from or against an account
 * @author james
 *
 */
public class AccountHistoryQuery
{
	/** Account number to look up */
	private String number;
	
	/**
	 * Default constructor for AccountQuery
	 */
	public AccountHistoryQuery()
	{
	}

	/**
	 * @return the number
	 */
	public final String getNumber()
	{
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public final void setNumber(String number)
	{
		this.number = number;
	}
}
