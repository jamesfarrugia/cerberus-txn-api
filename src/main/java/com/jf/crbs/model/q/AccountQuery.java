package com.jf.crbs.model.q;

/**
 * Query object used for querying accounts.  This is a simple base-case object
 * which can be easily extended to allow for more complex API-level querying
 * @author james
 *
 */
public class AccountQuery
{
	/** Account number to look up */
	private String number;
	
	/**
	 * Default constructor for AccountQuery
	 */
	public AccountQuery()
	{
	}

	/**
	 * @return the number
	 */
	public final String getNumber()
	{
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public final void setNumber(String number)
	{
		this.number = number;
	}
}
