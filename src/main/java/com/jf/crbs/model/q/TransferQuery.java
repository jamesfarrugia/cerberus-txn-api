package com.jf.crbs.model.q;

/**
 * Query to find a transfer by number
 * @author james
 *
 */
public class TransferQuery
{
	/** Account number to look up */
	private String number;
	
	/**
	 * Default constructor for AccountQuery
	 */
	public TransferQuery()
	{
	}

	/**
	 * @return the number
	 */
	public final String getNumber()
	{
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public final void setNumber(String number)
	{
		this.number = number;
	}
}
