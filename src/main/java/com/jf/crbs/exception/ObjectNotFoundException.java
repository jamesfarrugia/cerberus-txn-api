package com.jf.crbs.exception;

/**
 * A API exception thrown when a requested object is not found
 * @author james
 *
 */
public class ObjectNotFoundException extends ApiException
{
	/**
	 * Default constructor for ApiException
	 */
	public ObjectNotFoundException()
	{
		super("Object not found");
	}
	
	/**
	 * Default constructor for ApiException
	 * @param message error message
	 */
	public ObjectNotFoundException(String message)
	{
		super(message);
	}
	
	/**
	 * Default constructor for ApiException
	 * @param message error message
	 * @param cause exception root cause
	 */
	public ObjectNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
