package com.jf.crbs.exception;

/**
 * A general API exception
 * @author james
 *
 */
public class ApiException extends Exception
{
	/**
	 * Default constructor for ApiException
	 */
	public ApiException()
	{
		super("General API Exception");
	}
	
	/**
	 * Default constructor for ApiException
	 * @param message error message
	 */
	public ApiException(String message)
	{
		super(message);
	}
	
	/**
	 * Default constructor for ApiException
	 * @param message error message
	 * @param cause exception root cause
	 */
	public ApiException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
