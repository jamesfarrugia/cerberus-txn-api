package com.jf.crbs.exception;

/**
 * An API exception thrown when a requested business operation cannot be
 * completed
 * @author james
 *
 */
public class BusinessException extends ApiException
{
	/**
	 * Default constructor for ApiException
	 */
	public BusinessException()
	{
		super("Business error");
	}
	
	/**
	 * Default constructor for ApiException
	 * @param message error message
	 */
	public BusinessException(String message)
	{
		super(message);
	}
	
	/**
	 * Default constructor for ApiException
	 * @param message error message
	 * @param cause exception root cause
	 */
	public BusinessException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
