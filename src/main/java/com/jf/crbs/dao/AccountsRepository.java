package com.jf.crbs.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jf.crbs.model.Account;

/**
 * JPA repo implemented by spring data
 * @author james
 *
 */
@Repository
public interface AccountsRepository extends JpaRepository<Account, Long>
{

	/**
	 * Finds an account by public account number
	 * @param number public account number
	 * @return the matched account
	 */
	public Optional<Account> findOneAccountByNumber(String number);
}
