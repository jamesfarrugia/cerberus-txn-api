package com.jf.crbs.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jf.crbs.model.Transfer;

/**
 * JPA repo implemented by spring data
 * @author james
 *
 */
@Repository
public interface TransfersRepository extends JpaRepository<Transfer, Long>
{
	/**
	 * Finds a transfer by public transfer number
	 * @param number public transfer number
	 * @return the matched transfer
	 */
	public Optional<Transfer> findOneTransferByNumber(String number);
	
	/**
	 * Finds the transfers by source number
	 * @param number public account number of the transfer source
	 * @return the matching transfers
	 */
	public List<Transfer> findTransfersBySource(String number);
	
	/**
	 * Finds the transfers by destination number
	 * @param number public account number of the transfer destination
	 * @return the matching transfers
	 */
	public List<Transfer> findTransfersByDestination(String number);
}
