package com.jf.crbs.service;

import org.springframework.stereotype.Service;

import com.jf.crbs.exception.ObjectNotFoundException;

/**
 * A service to provide business methods for accounts
 * @author james
 *
 */
@Service
public class CurrencyService
{
	/**
	 * Gets the conversion rate from currency destination to target
	 * 
	 * @param source origin currency
	 * @param target target currency
	 * @return the rate between currencies
	 * @throws ObjectNotFoundException in case of an inexistent currency
	 */
	public float getRate(String source, String target)
	throws ObjectNotFoundException
	{
		return 1.0f;
	}
	
	/**
	 * Gets the conversion rate from the base currency to the target currency 
	 * 
	 * @param source origin currency
	 * @param target target currency
	 * @return the rate between currencies
	 * @throws ObjectNotFoundException in case of an inexistent currency
	 */
	public float getRate(String target)
	throws ObjectNotFoundException
	{
		return 1.0f;
	}
}
