package com.jf.crbs.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jf.crbs.dao.TransfersRepository;
import com.jf.crbs.exception.ApiException;
import com.jf.crbs.exception.BusinessException;
import com.jf.crbs.exception.ObjectNotFoundException;
import com.jf.crbs.model.Account;
import com.jf.crbs.model.Transfer;
import com.jf.crbs.model.c.AccountBalanceUpdate;
import com.jf.crbs.model.c.TransferCreation;
import com.jf.crbs.model.c.TransferUpdate;
import com.jf.crbs.model.q.AccountHistoryQuery;
import com.jf.crbs.model.q.AccountQuery;
import com.jf.crbs.model.q.TransferQuery;

/**
 * Service to manage business operational methods on transfers
 * @author james
 *
 */
@Service
@Transactional
public class TransfersService
{
	/** Service for account operations */
	@Autowired private AccountsService accounts;
	/** Service for currencies */
	@Autowired private CurrencyService currencies;
	
	/** Repository for transfers */
	@Autowired private TransfersRepository transfers;
	
	/**
	 * Creates a new transfer by logging a transfer creation event
	 * 
	 * @param creation the creation command
	 * @return the created transfer
	 * @throws ApiException in case of inexistent accounts, currencies, 
	 * insufficient balance or system failure
	 */
	public Transfer doCreateTransfer(TransferCreation creation)
	throws ApiException
	{
		AccountQuery query = new AccountQuery();
		
		query.setNumber(creation.getSource());
		Account source = accounts.getAccount(query);
		query.setNumber(creation.getDestination());
		Account dest = accounts.getAccount(query);
		
		/* create the transfer once we are sure accounts exist.  In case of
		 * other non-technical failures, we will log it as a failed transfer. */
		Transfer transfer = doCreateTransferRecord(source, dest, creation);
		
		/* get total transfer amount to deduct from origin */
		float total = creation.getAmount() + creation.getFee();

		/* if amount not in origin account currency, then we need to convert
		 * from destination currency */
		float crossRate = 1.0f;
		if (!creation.isCcyAsSource())
			crossRate = currencies.getRate(source.getCcy(), dest.getCcy());
		
		total = total * crossRate;
		transfer.setRate(crossRate);
		
		/* check if we have enough funds */
		if (total > source.getBalance())
		{
			transfer.setStatus((short)1);
			transfers.save(transfer);
			throw new BusinessException("Insufficient balance");
		}
		
		/* log the transfer */
		transfers.save(transfer);
		
		/* move the funds */
		doTransferFunds(source, dest, creation, (long)total, crossRate);
		
		return transfer;
	}
	
	/**
	 * Creates a new transfer record from the source and destination, creation
	 * command 
	 * @param source source account
	 * @param destination destination account
	 * @param creation creation command
	 * @return the new transfer record
	 */
	private Transfer doCreateTransferRecord(Account source, Account destination,
			TransferCreation creation)
	{
		Transfer transfer = new Transfer();
		transfer.setSource(source.getNumber());
		transfer.setDestination(destination.getNumber());
		transfer.setCcyAsSource(creation.isCcyAsSource());
		transfer.setFee(creation.getFee());
		transfer.setAmount(creation.getAmount());
		transfer.setStatus((short)0);
		transfer.setTimestamp(Instant.now().getEpochSecond());
		transfer.setNumber(UUID.randomUUID().toString());
		
		return transfer;
	}
	
	/**
	 * Moves the funds between accounts.  The origin is debited with the amount
	 * plus the fee, while the destination is credited with the equivalent 
	 * amount.
	 * 
	 * @param source source account
	 * @param destination destination account
	 * @param creation original command
	 * @param total transfer total
	 * @param crossRate rate between account currencies
	 */
	private void doTransferFunds(Account source, Account destination,
			TransferCreation creation, long total, float crossRate)
	{
		/* debit source */
		AccountBalanceUpdate balUpdate = new AccountBalanceUpdate();
		balUpdate.setId(source.getId());
		balUpdate.setDelta(-total);
		accounts.doUpdateAccountBalance(balUpdate);
		
		/* credit destination */
		balUpdate.setId(destination.getId());
		long received = creation.getAmount();
		if (creation.isCcyAsSource())
			received = (long) (received * crossRate);
		balUpdate.setDelta(received);
		accounts.doUpdateAccountBalance(balUpdate);
	}
	
	/**
	 * Gets an individual transfer identified by the public transfer number
	 * 
	 * @param query the query with the public transfer number
	 * @return the transfer if found
	 * @throws ObjectNotFoundException 
	 */
	public Transfer getTransfer(TransferQuery query) 
	throws ObjectNotFoundException
	{
		return transfers.findOneTransferByNumber(query.getNumber()).
				orElseThrow(() -> 
					new ObjectNotFoundException("No such transfer"));
	}

	/**
	 * Updates an individual transfer identified by the public transfer number
	 * 
	 * @param number the public transfer number
	 * @param update the update to apply to the transfer
	 * @return the updated transfer if found and updated
	 * @throws ObjectNotFoundException 
	 */
	public Transfer doUpdateTransfer(String number, TransferUpdate update)
	throws ObjectNotFoundException
	{
		TransferQuery query = new TransferQuery();
		query.setNumber(number);
		Transfer transfer = getTransfer(query);
		
		transfer.setStatus(update.getStatus());
		transfer.setFee(update.getFee());
		
		return transfers.save(transfer);
	}
	
	/**
	 * Gets an individual account identified by the public account number
	 * 
	 * @param query the query to run, containing the public account number
	 * @return the account if found
	 * @throws ObjectNotFoundException 
	 */
	public List<Transfer> getAccountTransfers(AccountHistoryQuery query) 
	throws ObjectNotFoundException
	{
		List<Transfer> result = new ArrayList<>();
		
		result.addAll(transfers.findTransfersBySource(query.getNumber()));
		result.addAll(transfers.findTransfersByDestination(query.getNumber()));
		
		return result;
	}
}
