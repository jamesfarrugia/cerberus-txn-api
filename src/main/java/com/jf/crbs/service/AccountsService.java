package com.jf.crbs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jf.crbs.dao.AccountsRepository;
import com.jf.crbs.exception.ObjectNotFoundException;
import com.jf.crbs.model.Account;
import com.jf.crbs.model.c.AccountBalanceUpdate;
import com.jf.crbs.model.q.AccountQuery;

/**
 * A service to provide business methods for accounts
 * @author james
 *
 */
@Service
public class AccountsService
{
	/** Repository for accounts */
	@Autowired private AccountsRepository accounts;
	
	/**
	 * Gets an individual account identified by the public account number
	 * 
	 * @param query the query to run, containing the public account number
	 * @return the account if found
	 * @throws ObjectNotFoundException 
	 */
	public Account getAccount(AccountQuery query) 
	throws ObjectNotFoundException
	{
		return accounts.findOneAccountByNumber(query.getNumber()).
				orElseThrow(() -> 
					new ObjectNotFoundException("No such account"));
	}
	
	/**
	 * Updates the balance of an account
	 * @param update the update command which instructs an amount to be added
	 * to the current balance.  The amount may be negative
	 */
	public void doUpdateAccountBalance(AccountBalanceUpdate update)
	{
		Account account = accounts.getOne(update.getId());
		account.setBalance(account.getBalance() + update.getDelta());
		accounts.save(account);
	}
}
