#!/bin/sh

echo "*-----------------*"
echo "|Fund transfer API|"
echo "*-----------------*"

echo "\nDetails of account 1"
curl http://127.0.0.1:8080/accounts/account/40054325
echo "\n\nDetails of account 2"
curl http://127.0.0.1:8080/accounts/account/40054895

echo "\n\n---------------\n# Small transfer, no fee"
curl -H "Content-Type: application/json" -X POST -d '{"source":40054325, "destination":40054895, "fee":0, "amount":10000, "ccyAsSource":true}' http://localhost:8080/transfers/transfer

echo "\nUpdated details of account 1"
curl http://127.0.0.1:8080/accounts/account/40054325
echo "\n\nUpdated details of account 2"
curl http://127.0.0.1:8080/accounts/account/40054895

echo "\n\n---------------\n# Small transfer, with fee"
curl -H "Content-Type: application/json" -X POST -d '{"source":40054325, "destination":40054895, "fee":100, "amount":10000, "ccyAsSource":true}' http://localhost:8080/transfers/transfer

echo "\nUpdated details of account 1"
curl http://127.0.0.1:8080/accounts/account/40054325
echo "\n\nUpdated details of account 2"
curl http://127.0.0.1:8080/accounts/account/40054895

echo "\n\n---------------\n# Large transfer, no fee"
curl -H "Content-Type: application/json" -X POST -d '{"source":40054325, "destination":40054895, "fee":0, "amount":50000, "ccyAsSource":true}' http://localhost:8080/transfers/transfer

echo "\nUpdated details of account 1"
curl http://127.0.0.1:8080/accounts/account/40054325
echo "\n\nUpdated details of account 2"
curl http://127.0.0.1:8080/accounts/account/40054895

echo "\n\n---------------\n# List of transfers for account 1"
curl http://localhost:8080/transfers/account/40054325

echo "\n\n"
